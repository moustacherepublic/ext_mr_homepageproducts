<?php
/**
 * Catalog Product Bestseller Block
 *
 * @author Amasty Team
 */
class MR_Homepageproducts_Block_Featured extends Mage_Catalog_Block_Product_New
{
    /**
     * Get Key pieces for caching block content
     *
     * @return array
     */
    public function getCacheKeyInfo()
    {
        return array(
            'MR_HOME_PRODUCT_FEATURED',
            Mage::app()->getStore()->getId(),
            Mage::getDesign()->getPackageName(),
            Mage::getDesign()->getTheme('template'),
            Mage::getSingleton('customer/session')->getCustomerGroupId(),
            'template' => $this->getTemplate(),
            $this->getProductsCount()
        );
    }


    protected function _getProductCollection()
    {
        $featuredCateId = Mage::getStoreConfig('mr_homepageproducts/general/featured_id');
        $featuredCat = Mage::getModel('catalog/category')->load($featuredCateId);

        if($featuredCat && $featuredCat->getId()){
            /** @var $collection Mage_Catalog_Model_Resource_Product_Collection */
            $collection = Mage::getResourceModel('catalog/product_collection');
            $collection = $this->_addProductAttributesAndPrices($collection)
                ->addStoreFilter()
                ->addAttributeToFilter('status',  array(Mage_Catalog_Model_Product_Status::STATUS_ENABLED))
                ->addCategoryFilter($featuredCat)
                ->setVisibility(Mage::getSingleton('catalog/product_visibility')->getVisibleInCatalogIds())
                ->setPageSize($this->getProductsCount())
                ->setCurPage(1);
           Mage::getSingleton('cataloginventory/stock')->addInStockFilterToCollection($collection);
            return $collection;
        }else{
            return false;
        }


    }

    public function getCacheLifetime()
    {
        return 0;
    }
}
