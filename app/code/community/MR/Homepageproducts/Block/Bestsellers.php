<?php
/**
 * Catalog Product Bestseller Block
 *
 * @author Amasty Team
 */
class MR_Homepageproducts_Block_Bestsellers extends Mage_Catalog_Block_Product_New
{
    /**
     * Get Key pieces for caching block content
     *
     * @return array
     */
    public function getCacheKeyInfo()
    {
        return array(
            'MR_HOME_PRODUCT_BESTSELLERS',
            Mage::app()->getStore()->getId(),
            Mage::getDesign()->getPackageName(),
            Mage::getDesign()->getTheme('template'),
            Mage::getSingleton('customer/session')->getCustomerGroupId(),
            'template' => $this->getTemplate(),
            $this->getProductsCount()
        );
    }


    protected function _getProductCollection()
    {
        $collection = Mage::getResourceModel('mr_homepageproducts/reports_product_collection');
        $collection = $this->_addProductAttributesAndPrices($collection)
            ->addStoreFilter()
            ->addOrderedQty()
            ->addAttributeToFilter('status',  array(Mage_Catalog_Model_Product_Status::STATUS_ENABLED))
            ->setVisibility(Mage::getSingleton('catalog/product_visibility')->getVisibleInCatalogIds())
            ->setOrder('ordered_qty', 'desc')
            ->setPageSize($this->getProductsCount())
            ->setCurPage(1);
        Mage::getSingleton('cataloginventory/stock')->addInStockFilterToCollection($collection);
        return $collection;
    }

}
