<?php

$this->startSetup();

try{
    $existingCat = Mage::getModel('catalog/category')->loadByAttribute('url_key','mr-home-featured');
    if( !$existingCat || !$existingCat->getId()){
        $category = Mage::getModel('catalog/category');
        $category->setName('Home Page Featured');
        $category->setUrlKey('mr-home-featured');
        $category->setIsActive(0);
        $parentCategory = Mage::getModel('catalog/category')->load(2);
        $category->setPath($parentCategory->getPath());
        $category->save();

        $this->setConfigData('mr_homepageproducts/general/featured_id', $category->getId());
    }
} catch(Exception $e) {
    Mage::logException($e);
}

$this->endSetup();
